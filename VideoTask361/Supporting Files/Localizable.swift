//
//  LocalizableProtocol.swift
//  VideoTask361
//
//  Created by omer kantar on 10/10/21.
//

import Foundation


public enum Localizable: String {
    case detailButton = "detail.button"
    case errorTitle = "error.alert.title"
    case okButton = "error.alert.ok"
    case play = "play.button"
    case pause = "pause.button"
    case close = "close.button"
    case videoLoop = "loop.video.title"
    
    
    public var text: String {
        return NSLocalizedString(rawValue, comment: "")
    }
}

