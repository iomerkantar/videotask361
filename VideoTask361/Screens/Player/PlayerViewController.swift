//
//  PlayerViewController.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit
import AVKit

final class PlayerViewController: UIViewController, ViewControllerProtocols, PlayerViewModelDelegate {
    
    typealias ContentView = PlayerContentView
    typealias ViewModel = PlayerViewModelProtocol
    
    @IBOutlet weak var contentView: ContentView!
    public var viewModel: PlayerViewModelProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.alertDelegate = self
        viewModel.build()
    }
    
    // MARK: - Action
    @IBAction private func dismissButtonTapped() {
        contentView.pause()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - PlayerViewModelDelegate
    public func playerBuild(url: URL, title: String) {
        contentView.configure(videoURL: url, title: title)
    }
    
    public func playerBuild(player: AVPlayer, title: String) {
        contentView.configure(player: player, title: title)
    }
}
