//
//  PlayerBuilder.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit
import AVFoundation


final class PlayerBuilder {
    
    private static func viewController(url: URL?, player: AVPlayer?, title: String) -> PlayerViewController {
        let viewController = PlayerViewController.make(identifier: .player, type: PlayerViewController.self) 
        let viewModel = PlayerViewModel(url: url, player: player, title: title)
        viewModel.delegate = viewController
        viewController.viewModel = viewModel
        return viewController
    }
    
    public static func make(url: URL, title: String) -> PlayerViewController {
        let viewController = viewController(url: url, player: nil, title: title)
        return viewController
    }
    
    public static func make(player: AVPlayer, title: String) -> PlayerViewController {
        let viewController = viewController(url: nil, player: player, title: title)
        return viewController
    }
}
