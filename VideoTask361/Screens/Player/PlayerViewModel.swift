//
//  PlayerViewModel.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit
import AVFoundation

public protocol PlayerViewModelDelegate: AnyObject {
    func playerBuild(url: URL, title: String)
    func playerBuild(player: AVPlayer, title: String)
}

public protocol PlayerViewModelProtocol: AnyObject {
    func build()
}

final class PlayerViewModel: PlayerViewModelProtocol {
    
    private var url: URL?
    private var player: AVPlayer?
    private var title: String
    public weak var delegate: PlayerViewModelDelegate!
    
    public init(url: URL? = nil, player: AVPlayer? = nil, title: String) {
        self.url = url
        self.player = player
        self.title = title
    }
    
    // MARK: - PlayerViewModelProtocol
    func build() {
        if let url = url {
            delegate.playerBuild(url: url, title: title)
        } else if let player = player {
            delegate.playerBuild(player: player, title: title)
        }
    }
    
}

