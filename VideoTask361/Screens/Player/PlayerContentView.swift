//
//  PlayerContentView.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit
import AVFoundation

final class PlayerContentView: UIView, PlayerViewDelegate {
    
    @IBOutlet private var playerView: PlayerView!
    @IBOutlet private var slider: UISlider!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var playButton: UIButton!
    @IBOutlet private var closeButton: UIButton!
    @IBOutlet private var actionView: UIView!
    @IBOutlet private var timeWatchedLabel: UILabel!
    @IBOutlet private var timeVideoLabel: UILabel!

    public weak var alertDelegate: AlertErrorDelegate!
    private var timer: Timer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        timeVideoLabel.text = ""
        timeWatchedLabel.text = ""
        timeVideoLabel.textColor = .white
        timeWatchedLabel.textColor = .white
        actionView.designComponent()
        slider.value = 0.0
        playerView.backgroundColor = .black
        backgroundColor = .black
        playerView.delegate = self
        slider.tintColor = .tint
        playButton.setTitleColor(.tint, for: .normal)
        titleLabel.textColor = .white
        closeButton.setTitle(Localizable.close.text, for: .normal)
    }
    
    private func configureAndPlay(title: String) {
        titleLabel.text = title
        playerView.play()
        playStatusChanged(true)
    }
    
    public func configure(player: AVPlayer, title: String) {
        playerView.load(player: player)
        configureAndPlay(title: title)
    }
    
    public func configure(videoURL: URL, title: String) {
        playerView.load(url: videoURL)
        configureAndPlay(title: title)
    }
    
    private func playStatusChanged(_ isPlaying: Bool) {
        let title = isPlaying ? Localizable.pause.text : Localizable.play.text
        playButton.setTitle(title, for: .normal)
        startBackgroundTimer()
    }
    
    public func pause() {
        playerView.pause()
        playStatusChanged(false)
    }
    
    // MARK: - Actions
    @IBAction private func playButtonTapped() {
        let isPlaying = playerView.isPlaying
        playStatusChanged(!isPlaying)
        if isPlaying {
            playerView.pause()
        } else {
            playerView.play()
        }
    }
    
    @IBAction private func sliderValueChanged() {
        startBackgroundTimer()
        playerView.sliderProgressValueChanged(slider.value)
        loopVideoIfNeeded()
    }
    
    @IBAction private func backgroundTapped() {
        actionViewAnimating(hidden: false)
        startBackgroundTimer()
    }
    
    private func loopVideoIfNeeded() {
        guard slider.value == 1.0, app.settings.videoLoopEnabled else {
            return
        }
        slider.value = 0.0
        playerView.sliderProgressValueChanged(0.0)
        playerView.play()
    }
    
    // MARK: - backgroun timer
    private func startBackgroundTimer() {
        stopBackgroundTimer()
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(backgroundTimerFinished), userInfo: nil, repeats: false)
    }
    
    private func stopBackgroundTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc private func backgroundTimerFinished() {
        stopBackgroundTimer()
        actionViewAnimating(hidden: true)
    }
    
    private func actionViewAnimating(hidden: Bool) {
        let alpha: CGFloat = hidden ? 0.0 : 1.0
        guard self.actionView.alpha != alpha else {
            return
        }
        UIView.animate(withDuration: 0.2) {
            self.actionView.alpha = alpha
            self.closeButton.alpha = alpha
        }
    }
    
    // MARK: - PlayerViewDelegate
    func playerViewPlayValueChanged(isPlaying: Bool) {
        playStatusChanged(isPlaying)
    }
    
    func playerViewCouldNotLoadVideo(error: Error) {
        alertDelegate.showAlert(error: error)
    }
    
    func playerViewPeriodicTimed(player: AVPlayer, progress: Float) {
        if !slider.isTracking {
            slider.value = progress
        }
        loopVideoIfNeeded()
        let timeWatched = player.currentTime().seconds.formatTime
        timeWatchedLabel.text = timeWatched
        if timeVideoLabel.text ?? "" == "",
           let currentItem = player.currentItem {
            let timeLeft = currentItem.duration.seconds.formatTime
            timeVideoLabel.text = timeLeft
        }
    }

}
