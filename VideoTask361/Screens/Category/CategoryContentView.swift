//
//  CategoryContentView.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

public protocol CategoryContentViewDelegate: TitleStackViewDelegate {
    
}

final class CategoryContentView: UIView {

    @IBOutlet private var loopVideoSwitch: UISwitch!
    @IBOutlet private var loopVideoLabel: UILabel!
    @IBOutlet private var titleStackView: TitleStackView!
    @IBOutlet private var activityIndicatorView: UIActivityIndicatorView!
    
    public weak var delegate: CategoryContentViewDelegate! {
        didSet {
            titleStackView?.delegate = delegate
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loopVideoSwitch.tintColor = .tint
        loopVideoLabel.text = Localizable.videoLoop.text
        loopVideoSwitch.isOn = app.settings.videoLoopEnabled
    }
    
    // MARK: -
    public func build(titles: [String]) {
        titleStackView.build(titles: titles)
    }
    
    public func activityIndicatorAnimated(_ animated: Bool) {
        activityIndicatorView.isHidden = !animated
        if animated {
            activityIndicatorView.startAnimating()
        } else {
            activityIndicatorView.stopAnimating()
        }
    }

    
    @IBAction private func loopVideoSwitchValueChanged() {
        app.settings.videoLoopEnabled = loopVideoSwitch.isOn
    }
}
