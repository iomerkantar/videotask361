//
//  CategoryBuilder.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit


final class CategoryBuilder: BuilderProtocol {
    public static func make() -> CategoryViewController {
        let viewModel = CategoryViewModel(config: appNetwork.config, session: appNetwork.session)
        let storyboard = UIStoryboard(name: "Category", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Category") as! CategoryViewController
        viewController.viewModel = viewModel
        viewModel.delegate = viewController
        return viewController
    }
}
