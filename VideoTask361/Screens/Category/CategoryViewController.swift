//
//  CategoryViewController.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

final class CategoryViewController: UIViewController, ViewControllerProtocols, CategoryViewModelDelegate, CategoryContentViewDelegate {
    
    typealias ContentView = CategoryContentView
    typealias ViewModel = CategoryViewModelProtocol
    
    @IBOutlet weak var contentView: ContentView!
    
    var viewModel: CategoryViewModelProtocol!
    
    private var videoListViewController: VideoListDataPassProtocol!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.delegate = self
        contentView.activityIndicatorAnimated(true)
        viewModel.loadCategories()
    }
    
    // MARK - prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let videoListViewController as VideoListViewController:
            self.videoListViewController = videoListViewController
        default: break
        }
    }
    
    
    // MARK: - CategoryViewModelDelegate
    
    func loading(visible: Bool) {
        DispatchQueue.main.async {
            self.contentView.activityIndicatorAnimated(visible)
        }
    }
    
    func didLoadCategories(_ categories: [CategoryPresentation]) {
        let titles = categories.map { $0.name }
        DispatchQueue.main.async {
            self.contentView.build(titles: titles)
        }
    }
    
    // MARK: - TitleStackViewDelegate
    func titleStackViewDidSelect(title: String) {
        let videos = viewModel.getVideos(category: title)
        self.videoListViewController.build(videos: videos)

    }
}
