//
//  CategoryViewModel.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

public protocol CategoryViewModelDelegate: ViewModelDelegate {
    func didLoadCategories(_ categories: [CategoryPresentation])
}

public protocol CategoryViewModelProtocol: AnyObject {
    func getVideos(category: String) -> [VideoModel]
    func loadCategories()
}

final class CategoryViewModel: BaseViewModel, CategoryViewModelProtocol {
    
    // Delegate uses for the data pass
    public weak var delegate: CategoryViewModelDelegate!
   
    // that relates protocol, because if that provider uses VideoProvider class, I can access to request function while videoProvider is used but it is so terrible.
    private var videoProvider: VideoProviderProtocol!
    
    // categories
    public var categories: [CategoryPresentation] = []
    
    // MARK: - init
    override init(config: WebServiceConfig, session: URLSession) {
        super.init(config: config, session: session)
        videoProvider = VideoProvider(session: session, config: config)
    }
    
    // MARK: - load categories
    public func loadCategories() {
        delegate.loading(visible: true)
        videoProvider.loadCategories { [weak self] (result) in
            guard let self = self else {
                return
            }
            self.delegate.loading(visible: false)
            switch result {
            case .success(let categories):
                self.categories = categories.map { CategoryPresentation(category: $0) }
                self.delegate.didLoadCategories(self.categories)
            case .failure(let error):
                self.delegate.showError(error)
            }
        }
    }
    
    func getVideos(category: String) -> [VideoModel] {
        return categories.first(where: { $0.category.name == category })?.videos ?? []
    }
    
}


public final class CategoryPresentation {
    public let category: CategoryModel
    public init(category: CategoryModel) {
        self.category = category
    }
    public var name: String {
        return category.name
    }
    public var videos: [VideoModel] {
        return category.videos
    }
}
