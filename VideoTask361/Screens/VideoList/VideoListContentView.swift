//
//  VideoListContentView.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

public protocol VideoListContentViewDelegate: VideoTableViewCellDelegate {
    
}

final class VideoListContentView: UIView, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet private var tableView: UITableView!
    public weak var delegate: VideoListContentViewDelegate!

    private let cellIdentifier = "VideoCell"
    
    private var videoList: [VideoTableCellDataProtocol]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        buildTableView()
    }
    
    
    // MARK: - Table Build
    private func buildTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.allowsMultipleSelection = false
        tableView.allowsSelection = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.reloadData()
    }
    
    public func configure(videos: [VideoTableCellDataProtocol]) {
        self.videoList = videos
        tableView.reloadData()
    }
    
    
    // MARK: - Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videoList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? VideoTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = delegate
        cell.configure(video: videoList[indexPath.row])
        return cell
    }
    
    // MARK: - Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
   
}
