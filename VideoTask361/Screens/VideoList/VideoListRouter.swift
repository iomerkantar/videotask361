//
//  VideoListRouter.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit
import AVFoundation

public protocol VideoListRouterProtocol: AnyObject {
    func presentPlayer(player: AVPlayer, title: String)
}


final class VideoListRouter: VideoListRouterProtocol {
    
    private weak var viewController: UIViewController?
    
    public init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func presentPlayer(player: AVPlayer, title: String) {
        let playerController = PlayerBuilder.make(player: player, title: title)
        playerController.modalPresentationStyle = .fullScreen
        playerController.modalTransitionStyle = .crossDissolve
        viewController?.present(playerController, animated: true, completion: nil)
    }
}
