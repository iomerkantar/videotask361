//
//  VideoListViewController.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit
import AVKit

public protocol VideoListDataPassProtocol {
    func build(videos: [VideoModel])
}

final class VideoListViewController: UIViewController, ViewControllerProtocols, VideoListViewModelDelegate, VideoListDataPassProtocol, ViewControllerRouterProtocol, VideoListContentViewDelegate {
    
    typealias ContentView = VideoListContentView
    typealias ViewModel = VideoListViewModelProtocol
    typealias Router = VideoListRouterProtocol

    @IBOutlet weak var contentView: VideoListContentView!
    public var viewModel: VideoListViewModelProtocol!
    public var router: VideoListRouterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.delegate = self
    }
    
    // MARK: - VideoListDataPassProtocol
    public func build(videos: [VideoModel]) {
        self.viewModel = VideoListViewModel(videos: videos, delegate: self)
        self.router = VideoListRouter(viewController: self)
        DispatchQueue.main.async {
            self.viewModel.loadVideos()
        }
    }
    
    // MARK: - VideoListViewModelDelegate
    func didLoadVideos(_ videos: [VideoTableCellDataProtocol]) {
        self.contentView.configure(videos: videos)
    }
    
    // MARK: - VideoListContentViewDelegate
    func videoTableViewCell(video: VideoTableCellDataProtocol, willPlay player: AVPlayer) {
        router.presentPlayer(player: player, title: video.title)
    }
    
    func videoTableViewCellDidNotLoadVideo(error: Error) {
        showAlert(error: error)
    }
}
