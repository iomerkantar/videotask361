//
//  VideoListViewModel.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit
import AVFoundation

public protocol VideoListViewModelDelegate: AnyObject {
    func didLoadVideos(_ videos: [VideoTableCellDataProtocol])
}

public protocol VideoListViewModelProtocol: AnyObject {
    func loadVideos()
}

final class VideoListViewModel:  VideoListViewModelProtocol {
    
    private let videos: [VideoModel]
    private weak var delegate: VideoListViewModelDelegate!
    
    public init(videos: [VideoModel], delegate: VideoListViewModelDelegate) {
        self.videos = videos
        self.delegate = delegate
    }
    
    public func loadVideos() {
        let providers = videos.compactMap { VideoCellPresentation(video: $0) }
        delegate.didLoadVideos(providers)
    }
    
}



final class VideoCellPresentation: VideoTableCellDataProtocol {
    
    public var title: String
    public var description: String
    public var thumbImageURL: URL?
    public var player: AVPlayer? = nil
    public let video: VideoModel
    public var isShowedDetail: Bool = false

    
    public init(video: VideoModel) {
        self.video = video
        self.title = video.title
        self.description = video.description ?? ""
        self.thumbImageURL = video.imageUrl?.url
        let videoURL = video.urls?.first?.url
        if let url = videoURL {
            self.player = AVPlayer(url: url)
        }
    }
    
}
