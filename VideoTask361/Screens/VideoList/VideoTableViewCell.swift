//
//  VideoTableViewCell.swift
//  VideoTask361
//
//  Created by omer kantar on 10/10/21.
//

import UIKit
import AVFoundation

public protocol VideoTableCellDataProtocol: AnyObject {
    var title: String { get }
    var description: String { get }
    var thumbImageURL: URL? { get }
    var isShowedDetail: Bool { set get }
    var player: AVPlayer? { get }
}

public protocol VideoTableViewCellDelegate: AnyObject {
    func videoTableViewCellDidNotLoadVideo(error: Error)
    func videoTableViewCell(video: VideoTableCellDataProtocol, willPlay player: AVPlayer)
}

final class VideoTableViewCell: UITableViewCell, PlayerViewDelegate {
 
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var thumbImageView: UIImageView!
    @IBOutlet private var playerView: PlayerView!
    @IBOutlet private var containerView: UIView!
    
    public weak var delegate: VideoTableViewCellDelegate!

    
    private var playerTapGestureRecognizer: UITapGestureRecognizer!
    private var imageTapGestureRecognizer: UITapGestureRecognizer!
    private var descriptionTapGestureRecognizer: UITapGestureRecognizer!

    private var videoData: VideoTableCellDataProtocol!
    private var maxShortDescriptionLenght: Int = 50
    
    // MARK: - Layout
    override func awakeFromNib() {
        super.awakeFromNib()
        playerView.delegate = self
        contentView.backgroundColor = .clear
        containerView.designComponent()
        backgroundColor = .clear
        selectedBackgroundView = UIView()
        descriptionTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(descriptionLabelTapped))
        descriptionLabel.isUserInteractionEnabled = true
        descriptionLabel.addGestureRecognizer(descriptionTapGestureRecognizer)
        playerView.backgroundColor = .black
        thumbImageView.isUserInteractionEnabled = true
        imageTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(thumbImageViewTapped))
        thumbImageView.addGestureRecognizer(imageTapGestureRecognizer)
        playerTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(thumbImageViewTapped))
        playerView.addGestureRecognizer(playerTapGestureRecognizer)
        playerView.isUserInteractionEnabled = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        descriptionLabel.text = nil
        thumbImageView.image = nil
        playerView.isHidden = true
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        let scale: CGFloat = highlighted ? 0.98 : 1.0
        UIView.animate(withDuration: 0.2) {
            self.containerView.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
    }

    // MARK: - Configure
    public func configure(video: VideoTableCellDataProtocol) {
        self.videoData = video
        titleLabel?.text = video.title
        thumbImageView.isHidden = false
        thumbImageView?.load(from: video.thumbImageURL)
        if let player = video.player {
            playerView?.load(player: player)
            if player.currentTime().seconds > 0 {
                thumbImageView.isHidden = true
                playerView.isHidden = false
            }
        }
        if video.isShowedDetail || video.description.count < maxShortDescriptionLenght {
            descriptionLabel?.text = video.description
            descriptionLabel.isUserInteractionEnabled = false
        } else {
            descriptionLabel.isUserInteractionEnabled = true
            let description = video.description.prefix(maxShortDescriptionLenght)
            let attributedString = NSMutableAttributedString(string: String(description))
            attributedString.append(NSAttributedString(string: " " + Localizable.detailButton.text, attributes: [.foregroundColor: UIColor.tint]))
            descriptionLabel?.attributedText = attributedString
        }
    }
    
    // MARK: - Tap Gestures
    @objc private func thumbImageViewTapped() {
        let isPlaying = playerView.isPlaying
        if isPlaying {
            playerView.pause()
        } else if let video = videoData, let player = video.player {
            playerView.isHidden = false
            thumbImageView.isHidden = true
            delegate.videoTableViewCell(video: video, willPlay: player)
//            playerView.play()
        }
    }
    
    // MARK: - Player
    @objc private func playerViewTapped() {
        thumbImageViewTapped()
    }
    
    @objc private func descriptionLabelTapped() {
        guard !videoData.isShowedDetail,
                videoData.description.count > maxShortDescriptionLenght,
                let tableView = (superview as? UITableView) else {
            return
        }
        tableView.beginUpdates()
        videoData.isShowedDetail = true
        descriptionLabel.text = videoData.description
        tableView.endUpdates()
    }
    
    // MARK: - PlayerViewDelegate
    func playerViewCouldNotLoadVideo(error: Error) {
        delegate.videoTableViewCellDidNotLoadVideo(error: error)
    }
    
}
