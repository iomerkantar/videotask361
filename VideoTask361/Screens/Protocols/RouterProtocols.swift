//
//  RouterProtocols.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

// Router structure
public protocol RouterProtocol: AnyObject {
    associatedtype Route
    var viewController: ViewControllerNavigationProtocol { get set }
    func navigate(to: Route)
}

public extension RouterProtocol {
    
    var navigationController: UINavigationController? {
        return viewController.navigationController
    }
    
    func dismiss(animated: Bool = true) {
        if let viewController = viewController as? UIViewController {
            viewController.dismiss(animated: animated, completion: nil)
        } else if navigationController != nil {
            if navigationController?.viewControllers.count == 1 {
                navigationController?.dismiss(animated: animated)
            } else {
                navigationController?.popToRootViewController(animated: animated)
            }
        }
    }
    
    func push(to viewController: UIViewController, animated: Bool = true) {
        navigationController?.pushViewController(viewController, animated: animated)
    }
}
