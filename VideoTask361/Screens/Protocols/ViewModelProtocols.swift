//
//  ViewModelProtocols.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation
import UIKit

public protocol ViewModelProtocol: AnyObject {
    var config: WebServiceConfig { set get }
    var session: URLSession { set get }
}


public protocol ViewModelDelegate: AnyObject {
    func showError(_ error: Error)
    func loading(visible: Bool)
}


public extension ViewModelDelegate where Self:  UIViewController {
    func showError(_ error: Error) {
        
    }
    func loading(visible: Bool) {
        
    }
}
