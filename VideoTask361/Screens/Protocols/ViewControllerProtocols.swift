//
//  ViewControllerProtocols.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

// Those protocols are structured ViewController. Therefore, the protocols should all be implemented on ViewController

public typealias ViewControllerProtocols = ViewControllerContentViewBindingProtocol & ViewControllerViewModelProtocol & ViewControllerNavigationProtocol


// to bind content view
// contentView as View of ViewController, for this reason content view must be weak reference because ViewController has a strong view for retain cycle
public protocol ViewControllerContentViewBindingProtocol: AnyObject {
    associatedtype ContentView: UIView
    var contentView: ContentView! { get set }
}


// view model
public protocol ViewControllerViewModelProtocol: AnyObject {
    associatedtype ViewModel
    var viewModel: ViewModel! { get set }
}

// Router
public protocol ViewControllerRouterProtocol: AnyObject {
    associatedtype Router
    var router: Router! { get set }
}

// Navigation 
public protocol ViewControllerNavigationProtocol: AnyObject {
    var navigationController: UINavigationController? { get }
}

// Loader
public protocol ViewControllerLoadProtocol: AnyObject {
    func loadingState(hidden: Bool)
}
