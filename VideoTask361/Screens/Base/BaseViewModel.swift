//
//  BaseViewModel.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

public class BaseViewModel: ViewModelProtocol {
    
    public var config: WebServiceConfig
    public var session: URLSession
    
    public init(config: WebServiceConfig, session: URLSession) {
        self.config = config
        self.session = session
    }
}
