//
//  AppNetwork.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

private struct WebServiceConfigDevelopment: WebServiceConfig {
    var baseURL: URL = "https://searchpanel-dot-tapstory.uc.r.appspot.com/".url!
    var environment: String = "Development"
}


public final class AppNetwork {
    
    public let config: WebServiceConfig
    public let session: URLSession
    
    public init() {
        self.session = URLSession.shared
        self.config = WebServiceConfigDevelopment()
    }
}
