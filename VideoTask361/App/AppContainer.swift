//
//  AppContainer.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

public let app = AppContainer()

public final class AppContainer {
 
    private let window: UIWindow
    
    public let network = AppNetwork()
    
    public let settings = AppSettings()
    
    public init() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .black
        window.makeKeyAndVisible()
    }
    
    public func start() {
        window.rootViewController = CategoryBuilder.make()
    }
}
