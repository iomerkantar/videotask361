//
//  PlayerView.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit
import AVFoundation

public enum PlayerError: Error {
    case didNotLoad
    
}

public protocol PlayerViewDelegate: AnyObject {
    func playerViewPlayValueChanged(isPlaying: Bool)
    func playerViewCouldNotLoadVideo(error: Error)
    func playerViewPeriodicTimed(player: AVPlayer, progress: Float)
}
public extension PlayerViewDelegate {
    func playerViewPlayValueChanged(isPlaying: Bool) { }
    func playerViewPeriodicTimed(player: AVPlayer, progress: Float) { }
}

public class PlayerView: UIView {
    
    private var playerLayer: AVPlayerLayer!
    private var player: AVPlayer!
    private var timeObserver: Any? = nil
    public weak var delegate: PlayerViewDelegate?
    
    // MARK: - Init
    public init() {
        super.init(frame: .zero)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - Layout
    public override func layoutSubviews() {
        super.layoutSubviews()
        guard let playerLayer = playerLayer, frame.size != playerLayer.frame.size else {
            return
        }
        playerLayer.frame = bounds
    }
    
    // MARK: - Setup
    private func setup() {
        playerLayer = AVPlayerLayer()
        playerLayer.frame = self.bounds
        layer.addSublayer(playerLayer)
    }
    
    
    // MARK: - Load
    public func load(url: URL?) {
        pause()
        guard let url = url else {
            delegate?.playerViewCouldNotLoadVideo(error: PlayerError.didNotLoad)
            return
        }
        player = AVPlayer(url: url)
        if let error = player.error {
            delegate?.playerViewCouldNotLoadVideo(error: error)
        }
        playerLayer.player = player
    }
    
    public func load(player: AVPlayer) {
        clear()
        self.player = player
        playerLayer.player = player
        if player.timeControlStatus == .playing {
            play()
        }
    }
    
    // MARK: - Controlls
    public var isPlaying: Bool {
        return player?.timeControlStatus == .playing
    }
    
    private func clear() {
        pause()
        player = nil
    }
    
    public func play() {
        player?.play()
        let interval = CMTime(seconds: 0.01, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        timeObserver = player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { [weak self] (elapsedTime) in
            guard let self = self else { return }
            self.periodicTimeObserverHandler()
        })
    }
    
    public func pause() {
        if let timeObserver = timeObserver, isPlaying {
            player.removeTimeObserver(timeObserver)
        }
        player?.pause()
    }
    
    public func sliderProgressValueChanged(_ progress: Float) {
        guard let duration = player?.currentItem?.duration else { return }
            let value = Float64(progress) * CMTimeGetSeconds(duration)
            let seekTime = CMTime(value: CMTimeValue(value), timescale: 1)
            player?.seek(to: seekTime )
    }
    
    
    // MARK: - Timer
    private func periodicTimeObserverHandler() {
        guard let currentTime = player?.currentTime() else { return }
        var progress: Float = 0.0
        let currentTimeInSeconds = CMTimeGetSeconds(currentTime)
        progress = Float(currentTimeInSeconds)
        if let currentItem = player?.currentItem {
            let duration = currentItem.duration
            if (CMTIME_IS_INVALID(duration)) {
                return
            }
            let currentTime = currentItem.currentTime()
            progress = Float(CMTimeGetSeconds(currentTime) / CMTimeGetSeconds(duration))
        }
        if let error = player.error {
            self.delegate?.playerViewCouldNotLoadVideo(error: error)
        }
        print("progress: ", progress)
        print("player rate: ", player.rate)
        self.delegate?.playerViewPeriodicTimed(player: player, progress: progress)
    }
    
}
