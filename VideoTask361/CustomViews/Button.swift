//
//  Button.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

public class Button: UIButton {
    
    public enum Style {
        case `default`, title, detail
    }
    
    private var style: Style = .default
    
    static func make(style: Style,
                     title: String? = nil) -> Button {
        let button = Button(type: .system)
        button.setTitle(title, for: .normal)
        button.style = style
        button.design()
        return button
    }
    
    private func design() {
        switch style {
        case .default: return
        case .title:
            setTitleColor(.tint, for: .normal)
        case .detail:
            setTitle(NSLocalizedString(Localizable.detailButton.text, comment: ""), for: .normal)
        }
    }
}
