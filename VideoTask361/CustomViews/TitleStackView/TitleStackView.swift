//
//  TitleStackView.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import UIKit

public protocol TitleStackViewDelegate: AnyObject {
    func titleStackViewDidSelect(title: String)
}

public final class TitleStackView: UIStackView {

    public weak var delegate: TitleStackViewDelegate!
    private var selectedButton: Button? = nil
    private var buttons: [Button]!
    
    private func removeButtons() {
        guard let buttons = buttons else {
            return
        }
        buttons.forEach { $0.removeFromSuperview() }
        arrangedSubviews.forEach { removeArrangedSubview($0) }
        self.buttons.removeAll()
    }
    
    public func build(titles: [String]) {
        removeButtons()
        let buttons = titles.compactMap { Button.make(style: .title, title: $0) }
        for (offset, button) in buttons.enumerated() {
            button.addTarget(self, action: #selector(titleButtonTapped(_:)), for: .touchUpInside)
            insertArrangedSubview(button, at: offset)
        }
        self.buttons = buttons
        if let first = buttons.first {
            titleButtonTapped(first)
        }
    }
    
    @objc private func titleButtonTapped(_ button: Button) {
        if let selectedButton = selectedButton {
            if selectedButton == button { return }
            design(button: selectedButton, selected: false)
        }
        guard let title = button.title(for: .normal) else {
            return
        }
        design(button: button, selected: true)
        delegate.titleStackViewDidSelect(title: title)
        self.selectedButton = button
    }
    
    private func design(button: Button, selected: Bool) {
        let fontSize: CGFloat = 20
        let font: UIFont = selected ? .systemFont(ofSize: fontSize, weight: .medium) : .systemFont(ofSize: fontSize, weight: .regular)
        button.titleLabel?.font = font
        button.setTitleColor(selected ? .tint : .gray, for: .normal)
    }
}
