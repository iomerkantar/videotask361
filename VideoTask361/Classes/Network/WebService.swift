//
//  WebService.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

public enum RequestMethod: String {
    case get, post, put, patch, delete
    public var rawValue: String {
        return "\(self)".uppercased()
    }
}

public protocol RequestTarget {
    var path: String { get }
    var method: RequestMethod { get }
    var parameters: [String: Any]? { get }
    var headers: [String: String] { get }
}

public extension RequestTarget {
    var parameters: [String: Any]? {
        return nil
    }
    var headers: [String: String] {
        return WebServiceConsts.headers
    }
}

public protocol WebService {
    associatedtype Target: RequestTarget
    var config: WebServiceConfig { get }
    var session: URLSession { get }
}

public enum Result<T> {
    case success(T)
    case failure(Error)
}

public extension WebService {
    func request<T: Decodable>(target: Target,
                               response: T.Type,
                               completion: @escaping (Result<T>) -> Void) {
        
        var urlRequest: URLRequest!
        do {
            urlRequest = try getUrlRequest(for: target)
        } catch let error {
            completion(.failure(error))
            return
        }
        let session = self.session
        let dataTask = session.dataTask(with: urlRequest, completionHandler: { (data, _, error) -> Void in
          if let data = data {
              do {
                  let result = try JSONDecoder().decode(response, from: data)
                  completion(.success(result))
              } catch let error {
                  completion(.failure(error))
              }
          } else if let error = error {
              completion(.failure(error))
          } else {
              completion(.failure(WebServiceError.notFoundData))
          }
        })
        dataTask.resume()

    }
    
    
    private func getUrlRequest(for target: Target) throws -> URLRequest {

        let headers = target.headers
        var postData: Data?
        if let parameters = target.parameters {
            postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
        }
        let path = target.path
        let baseUrl = config.baseURL.absoluteString
        let urlPath = baseUrl + path
        guard let url = urlPath.url else {
            throw WebServiceError.wrongUrl
        }
        let request = NSMutableURLRequest(url: url,
                                                cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: WebServiceConsts.timeoutInterval)
        request.httpMethod = target.method.rawValue
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        return request as URLRequest
    }
}
