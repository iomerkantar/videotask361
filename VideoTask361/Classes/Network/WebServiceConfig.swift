//
//  NetworkService.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

public protocol WebServiceConfig {
    var baseURL: URL { get }
    var environment: String { get }
}


public struct WebServiceConsts {
    
    static let headers: [String: String] = ["Content-Type": "application/json"]
    
    static let timeoutInterval: TimeInterval = 10.0
    
    
}
