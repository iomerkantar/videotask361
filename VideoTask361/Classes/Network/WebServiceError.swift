//
//  WebServiceError.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

public enum WebServiceError: Error {
    case wrongUrl
    case notFoundData
    case jsonMapping
     
}
