//
//  VideoProvider.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

public protocol VideoProviderProtocol: AnyObject {
    func loadCategories(result: @escaping (Result<[CategoryModel]>) -> Void)
}

public class VideoProvider: WebService, VideoProviderProtocol {
    
    public typealias Target = VideoTarget
    public var config: WebServiceConfig
    public var session: URLSession

    // MARK: - Init
    public init(session: URLSession, config: WebServiceConfig) {
        self.session = session
        self.config = config
    }
    
    // MARK: - request methods
    // load categories
    public func loadCategories(result: @escaping (Result<[CategoryModel]>) -> Void) {
        request(target: .devtask, response: [CategoryModel].self, completion: result)
    }
    
    // MARK: - Target
    public enum VideoTarget: RequestTarget {
        case devtask
        public var path: String {
            switch self {
            case .devtask:
                return "devtask"
            }
        }
        public var method: RequestMethod {
            switch self {
            case .devtask:
                return .get
            }
        }
    }
    
}
