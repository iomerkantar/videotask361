//
//  VideoModel.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

public struct VideoModel: Codable {
    public let title: String
    public let subtitle: String?
    public let description: String?
    public let urls: [String]?
    public let imageUrl: String?

    private enum CodingKeys: String, CodingKey {
        case title, subtitle, description
        case imageUrl = "thumb"
        case urls = "sources"
    }
}
