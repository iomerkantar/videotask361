//
//  ContentModel.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

public struct CategoryModel: Codable {
    public let name: String
    public let videos: [VideoModel]
}
