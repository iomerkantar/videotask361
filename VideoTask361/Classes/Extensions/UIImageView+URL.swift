//
//  UIImageView+URL.swift
//  VideoTask361
//
//  Created by omer kantar on 10/10/21.
//

import Foundation
import UIKit


public extension UIImageView {
    func load(from url: URL?, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = url else { return }
        contentMode = mode
        URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            guard let self = self,
                  error == nil,
                  let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                  let data = data,
                  let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.image = image
            }
        }.resume()
    }
}
