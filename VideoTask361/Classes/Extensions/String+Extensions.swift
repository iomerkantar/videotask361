//
//  String+Extensions.swift
//  VideoTask361
//
//  Created by omer kantar on 9/10/21.
//

import Foundation

public extension String {
    var url: URL? {
        let replaced = self.replacingOccurrences(of: "http://", with: "https://")
        return URL(string: replaced)
    }
}
