//
//  Double+Duration.swift
//  VideoTask361
//
//  Created by omer kantar on 11/10/21.
//

import Foundation


public extension Double {
    private var hoursMinutesSeconds: (hours: Int, minutes: Int, seconds: Int) {
        let secs = Int(self)
        let hours = secs / 3600
        let minutes = (secs % 3600) / 60
        let seconds = (secs % 3600) % 60
        return (hours, minutes, seconds)
    }
    
    var formatTime: String {
        let result = hoursMinutesSeconds
        let hoursString = "\(result.hours)"
        var minutesString = "\(result.minutes)"
        if minutesString.count == 1 {
            minutesString = "0\(result.minutes)"
        }
        var secondsString = "\(result.seconds)"
        if secondsString.count == 1 {
            secondsString = "0\(result.seconds)"
        }
        var time = "\(hoursString):"
        if result.hours >= 1 {
            time.append("\(minutesString):\(secondsString)")
        } else {
            time = "\(minutesString):\(secondsString)"
        }
        return time
    }
}
