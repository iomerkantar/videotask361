//
//  UIViewController+Storyboard.swift
//  VideoTask361
//
//  Created by omer kantar on 11/10/21.
//

import UIKit

// TODO: - Add Storyboard, Identifier
// Create a view controller with identifier


public extension UIViewController {
    private enum Stroyboard: String {
        case category = "Category", videList = "VideoList", player = "Player"
        
    }
    enum Identifier: String {
        case category = "Category", videoList = "VideoList", player = "Player"
        
        fileprivate var storyboard: UIStoryboard {
            let storyboard: Stroyboard
            switch self {
            case .category:
                storyboard = .category
            case .videoList:
                storyboard = .videList
            case .player:
                storyboard = .player
            }
            return UIStoryboard(name: storyboard.rawValue, bundle: nil)
        }
        
        fileprivate var viewController: UIViewController {
            return storyboard.instantiateViewController(withIdentifier: rawValue)
        }
    }
    
    static func make<V: UIViewController>(identifier: Identifier, type: V.Type) -> V {
        return identifier.viewController as! V
    }
}
