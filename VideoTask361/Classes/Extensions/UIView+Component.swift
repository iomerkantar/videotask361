//
//  UIView+Component.swift
//  VideoTask361
//
//  Created by omer kantar on 11/10/21.
//

import UIKit

public extension UIView {
    func designComponent() {
        addShadow()
        addCornerRadius()
    }
    
    func addShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 3.0
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.15
    }
    
    func addCornerRadius() {
        layer.cornerRadius = 15.0
    }
}
