//
//  UIViewController+Alert.swift
//  VideoTask361
//
//  Created by omer kantar on 11/10/21.
//

import UIKit

// TODO: - show pop-up, error with alert controller
// MARK: - AlertView

public typealias AlertActionCompletion = (_ title: String, _ index: Int) -> Void

public protocol AlertErrorDelegate: AnyObject {
    func showAlert(error: Error)
}

extension UIViewController: AlertErrorDelegate {
    
    func showAlertController(title: String? = nil,
                             message: String? = nil,
                             buttonTitles: [String],
                             actionCompletion: AlertActionCompletion? = nil) {
        
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for title in buttonTitles {
            let action = UIAlertAction(title: title, style: .default, handler: { (act: UIAlertAction) in
                if let actionCompletion = actionCompletion {
                    actionCompletion(title, alertController.actions.firstIndex(of: act)!)
                }
            })
            alertController.addAction(action)
        }
        
        alertController.view.tintColor = .tint
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    public func showAlert(error: Error) {
        showAlertController(title: Localizable.errorTitle.text, message: error.localizedDescription, buttonTitles: [Localizable.okButton.text], actionCompletion: nil)
    }
    
}
