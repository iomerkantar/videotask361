//
//  UIColor+Extensions.swift
//  VideoTask361
//
//  Created by omer kantar on 10/10/21.
//

import UIKit


public extension UIColor {
    static var tint: UIColor {
        return UIColor.init(red: 12/255.0, green: 242/255.0, blue: 138/255.0, alpha: 1)
    }
}
